package com.atlassian.indexer.application.task.stream;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.service.IndexService;
import com.atlassian.indexer.application.task.ImmutableItemToDocumentsFunction;
import com.atlassian.indexer.application.task.ItemToDocumentsFunction;
import com.atlassian.indexer.application.task.stream.command.CheckpointShardCommand;
import com.atlassian.indexer.application.task.stream.command.ProcessRecordsCommand;

public class StreamProcessorFactory implements IRecordProcessorFactory {
    private final IndexConfiguration indexConfiguration;
    private final String documentIdAttribute;
    private final IndexService indexService;

    public StreamProcessorFactory(final IndexConfiguration indexConfiguration, final String documentIdAttribute, final IndexService indexService) {
        this.indexConfiguration = indexConfiguration;
        this.documentIdAttribute = documentIdAttribute;
        this.indexService = indexService;
    }

    @Override
    public IRecordProcessor createProcessor() {
        final CheckpointShardCommand checkpointShardCommand = new CheckpointShardCommand();
        final ItemToDocumentsFunction itemToDocumentsFunction = ImmutableItemToDocumentsFunction.builder()
                .withIndex(indexConfiguration.getIndex())
                .withDocumentIdAttribute(documentIdAttribute)
                .withVersionAttribute(indexConfiguration.getVersionAttribute())
                .withVersionType(indexConfiguration.getVersionType())
                .build();
        final RecordsToDocumentMapFunction recordsToDocumentMapFunction = new RecordsToDocumentMapFunction(itemToDocumentsFunction);
        final ProcessRecordsCommand processRecordsCommand = new ProcessRecordsCommand(indexConfiguration, indexService, recordsToDocumentMapFunction);

        return new StreamProcessor(checkpointShardCommand, processRecordsCommand, indexConfiguration.getIndexName());
    }
}
