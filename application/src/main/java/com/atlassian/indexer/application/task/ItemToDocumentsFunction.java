package com.atlassian.indexer.application.task;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.atlassian.indexer.application.entity.BooleanField;
import com.atlassian.indexer.application.entity.Document;
import com.atlassian.indexer.application.entity.FieldBuilder;
import com.atlassian.indexer.application.entity.NumberField;
import com.atlassian.indexer.application.entity.ObjectField;
import com.atlassian.indexer.application.entity.StringField;
import com.atlassian.indexer.model.index.Index;
import com.atlassian.indexer.model.index.IndexProperties;
import org.immutables.value.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;

@Value.Immutable
@Value.Style(init = "with*", get = {"is*", "get*"})
public abstract class ItemToDocumentsFunction implements BiFunction<Document.Operation, Map<String, AttributeValue>, Set<Document>> {
    private static final Logger logger = LoggerFactory.getLogger(ItemToDocumentsFunction.class);

    public abstract Index getIndex();

    public abstract String getDocumentIdAttribute();

    public abstract Optional<String> getVersionAttribute();

    public abstract Optional<String> getVersionType();

    @Override
    public Set<Document> apply(final Document.Operation operation, final Map<String, AttributeValue> item) {
        final Set<Document> documents = new HashSet<>();

        getIndex().getMappings().forEach((type, mapping) -> {
            final String documentId = getDocumentId(item);
            final Document.Builder document = Document.builder(documentId, type, operation);
            addVersion(item, document);
            addFields(mapping, item, document);
            documents.add(document.build());
        });

        return documents;
    }

    private String getDocumentId(final Map<String, AttributeValue> image) {
        return Optional.ofNullable(image.get(getDocumentIdAttribute()))
                .map(AttributeValue::getS)
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Image missing document ID attribute %s", getDocumentIdAttribute())));
    }

    private void addVersion(final Map<String, AttributeValue> image, final Document.Builder document) {
        getVersionAttribute().ifPresent(attribute -> document.setVersion(Optional.ofNullable(image.get(attribute))
                .map(AttributeValue::getN)
                .map(Long::parseLong)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Image missing version attribute %s", attribute)))));

        getVersionType().ifPresent(document::setVersionType);
    }

    private void addFields(final IndexProperties properties,
                           final Map<String, AttributeValue> attributes,
                           final FieldBuilder fieldBuilder) {
        properties.getProperties().forEach((fieldName, field) ->
                Optional.ofNullable(attributes.get(fieldName)).ifPresent(attribute -> {
                    if (attribute.getBOOL() != null) {
                        fieldBuilder.addField(new BooleanField(fieldName, attribute.getBOOL()));
                    } else if (attribute.getN() != null) {
                        fieldBuilder.addField(new NumberField(fieldName, new BigDecimal(attribute.getN())));
                    } else if (attribute.getS() != null) {
                        fieldBuilder.addField(new StringField(fieldName, attribute.getS()));
                    } else if (attribute.getM() != null) {
                        if (field instanceof IndexProperties) {
                            final ObjectField.Builder objectField = ObjectField.builder(fieldName);

                            addFields((IndexProperties) field, attribute.getM(), objectField);

                            fieldBuilder.addField(objectField.build());
                        } else {
                            logger.error("Invalid mapping, index field {} is not an object", fieldName);
                        }
                    } else {
                        logger.info("Unsupported attribute type {}", attribute);
                    }
                }));
    }
}
