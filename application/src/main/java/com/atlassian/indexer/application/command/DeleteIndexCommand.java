package com.atlassian.indexer.application.command;

import com.atlassian.elasticsearch.client.Client;
import com.atlassian.elasticsearch.client.indices.DeleteIndexResponse;
import com.atlassian.indexer.application.configuration.tenacity.TenacityPropertyKeys;

import javax.ws.rs.core.Response;

import static com.atlassian.elasticsearch.client.ES.index;
import static com.atlassian.indexer.application.ErrorKeys.ErrorKey.ELASTICSEARCH_INDEX_DELETE_FAILED;

/**
 * A command to delete an index in elasticsearch.
 */
public class DeleteIndexCommand extends AbstractSearchCommand<Void> {
    private final Client searchClient;
    private final String indexName;

    public DeleteIndexCommand(final Client searchClient, final String indexName) {
        super(TenacityPropertyKeys.CREATE_INDEX);

        this.searchClient = searchClient;
        this.indexName = indexName;
    }

    @Override
    protected CommandResult<Void> run() {
        final DeleteIndexResponse deleteIndexResponse = searchClient.execute(index(indexName).delete()).join();

        if (!deleteIndexResponse.isStatusSuccess() && deleteIndexResponse.getStatusCode() != Response.Status.NOT_FOUND.getStatusCode()) {
            return CommandResult.error(ELASTICSEARCH_INDEX_DELETE_FAILED,
                    String.format("Error deleting index %s, response code %d", indexName, deleteIndexResponse.getStatusCode()));
        }

        return CommandResult.ok();
    }
}
