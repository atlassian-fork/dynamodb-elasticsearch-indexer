package com.atlassian.indexer.application.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.atlassian.indexer.application.exception.TableNotFoundException;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * Implementation of the ItemDao.
 */
public final class ItemDaoImpl implements ItemDao {
    private final AmazonDynamoDB client;
    private final String tableName;

    ItemDaoImpl(final AmazonDynamoDB client, final String tableName) {
        this.client = client;
        this.tableName = tableName;
    }

    @Override
    public TableDescription getTableDescription() {
        try {
            return client.describeTable(tableName).getTable();
        } catch (final ResourceNotFoundException tableNotFound) {
            throw new TableNotFoundException(tableName);
        }
    }

    @Override
    public long countItems() {
        return client.describeTable(tableName).getTable().getItemCount();
    }

    @Override
    public ScanResult getItems(@Nullable final Map<String, AttributeValue> lastKeyEvaluated) {
        final ScanRequest scanRequest = new ScanRequest()
                .withTableName(tableName)
                .withExclusiveStartKey(lastKeyEvaluated);

        return client.scan(scanRequest);
    }
}
