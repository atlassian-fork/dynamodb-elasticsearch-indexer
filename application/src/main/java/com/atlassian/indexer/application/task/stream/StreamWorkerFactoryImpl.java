package com.atlassian.indexer.application.task.stream;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.streamsadapter.AmazonDynamoDBStreamsAdapterClient;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.services.kinesis.metrics.interfaces.MetricsLevel;
import com.atlassian.indexer.application.configuration.IndexConfiguration;
import com.atlassian.indexer.application.service.IndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class StreamWorkerFactoryImpl implements StreamWorkerFactory {
    private static final Logger logger = LoggerFactory.getLogger(StreamWorkerFactoryImpl.class);

    private static final String WORKER_ID_PATTERN = "%s-worker-%s";

    private static final int MAX_RECORDS = 1000;

    private final AWSCredentialsProvider awsCredentialsProvider;
    private final AmazonDynamoDBStreamsAdapterClient streamsAdapterClient;
    private final IndexService indexService;
    private final AmazonDynamoDB dynamoDBClient;

    @Autowired
    public StreamWorkerFactoryImpl(final AWSCredentialsProvider awsCredentialsProvider,
                                   final AmazonDynamoDBStreamsAdapterClient streamsAdapterClient,
                                   final IndexService indexService,
                                   final AmazonDynamoDB dynamoDBClient) {
        this.awsCredentialsProvider = awsCredentialsProvider;
        this.streamsAdapterClient = streamsAdapterClient;
        this.indexService = indexService;
        this.dynamoDBClient = dynamoDBClient;
    }

    @Override
    public Worker createWorker(final String streamArn, final String documentIdAttribute, final IndexConfiguration indexConfiguration) {
        final String applicationName = indexConfiguration.getIndexName();
        final String workerId = String.format(WORKER_ID_PATTERN, applicationName, UUID.randomUUID());
        final StreamProcessorFactory processorFactory = new StreamProcessorFactory(indexConfiguration, documentIdAttribute, indexService);
        final KinesisClientLibConfiguration workerConfig = new KinesisClientLibConfiguration(applicationName, streamArn, awsCredentialsProvider, workerId)
                .withMaxRecords(MAX_RECORDS)
                .withInitialPositionInStream(InitialPositionInStream.TRIM_HORIZON)
                .withTableName(indexConfiguration.getStreamCheckpointTableName())
                .withMetricsLevel(MetricsLevel.NONE);

        logger.info("Index {}: Creating DynamoDB stream processor worker {} for stream {}", indexConfiguration.getIndexName(),
                workerConfig.getApplicationName(), workerConfig.getStreamName());

        return new Worker.Builder()
                .dynamoDBClient(dynamoDBClient)
                .recordProcessorFactory(processorFactory)
                .config(workerConfig)
                .kinesisClient(streamsAdapterClient)
                .build();
    }
}
