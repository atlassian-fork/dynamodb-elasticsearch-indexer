package com.atlassian.indexer.application.configuration.tenacity;

import com.atlassian.indexer.application.IndexerApplication;
import com.atlassian.indexer.application.configuration.IndexerConfiguration;
import com.google.common.collect.ImmutableMap;
import com.yammer.tenacity.core.bundle.BaseTenacityBundleConfigurationFactory;
import com.yammer.tenacity.core.config.TenacityConfiguration;
import com.yammer.tenacity.core.properties.TenacityPropertyKey;
import com.yammer.tenacity.core.properties.TenacityPropertyKeyFactory;

import java.util.Map;

/**
 * {@link BaseTenacityBundleConfigurationFactory} for the {@link IndexerApplication}.
 */
public class TenacityBundleConfigurationFactory extends BaseTenacityBundleConfigurationFactory<IndexerConfiguration> {
    @Override
    public TenacityPropertyKeyFactory getTenacityPropertyKeyFactory(final IndexerConfiguration configuration) {
        return new TenacityPropertyKeysFactory();
    }

    @Override
    public Map<TenacityPropertyKey, TenacityConfiguration> getTenacityConfigurations(final IndexerConfiguration configuration) {
        return ImmutableMap.<TenacityPropertyKey, TenacityConfiguration>builder()
                .put(TenacityPropertyKeys.BULK_INDEX, configuration.getElasticSearchTenacityConfig())
                .put(TenacityPropertyKeys.CREATE_INDEX, configuration.getElasticSearchTenacityConfig())
                .build();
    }
}
